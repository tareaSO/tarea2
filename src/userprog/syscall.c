#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"


// Implementation Parameters
// =========================
static int WRITE_SIZE=128;
static int VALIDEXIT=-10;



typedef int pid_t;

inline
int
syscall_write(struct intr_frame *f)
{
  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)

  //FIXME: This is (probably) displaced because of the -12 on process.c:442 (setup_stack)
  //int file_descriptor =         p[1];
  //char        *buffer = (char*) p[2];
  //int            size = (int)   p[3];  // huge size may cause overflow

  int file_descriptor =         p[5];
  char        *buffer = (char*) p[6];
  int            size = (int)   p[7];  // may overflow signed int!!

  switch(file_descriptor)
  {
    case STDIN_FILENO:
      // Inform that no data was written
      f->eax=0;
      // REVIEW: Should that process be terminated?
      return 2;

    case STDOUT_FILENO:
    {
      // Write in chunks of WRITE_SIZE
      //   putbuf (src/lib/kernel/console.c) locks the console,
      //   we should avoid locking it too often or for too long
      int remaining = size;
      while(remaining > WRITE_SIZE)
      {
        // Write a chunk
        putbuf(buffer, WRITE_SIZE);
        // Advance buffer pointer
        buffer    += WRITE_SIZE;
        remaining -= WRITE_SIZE;
      }
      // Write all the remaining data
      putbuf(buffer, remaining);

      // Inform the amount of data written
      f->eax=(int)size;
      return 0;
    }

    default:
      printf("syscall: write call not implemented for files\n");
      return 1;
  }

  return 1;  // Unreachable, but compiler complains
}

void syscall_exit(int status){

thread_current()->exit_status=status;
if((thread_current()->parent)!=NULL){
  if((thread_current()->parent)->waiting_for!=NULL){
  if((thread_current()->parent)->waiting_for==thread_current()->tid){
VALIDEXIT=status;
struct thread *parent=thread_current()->parent;
parent->exit_status_hijo=VALIDEXIT;

 
// printf("%s\n",(thread_current()->parent)->name);
 list_remove(&thread_current()->child_elem);
 sema_up (&(thread_current()->parent)->sema_wait);
}
  }
}

thread_exit();


}

pid_t syscall_execute(char* file_name){


  
 int pid= process_execute(file_name);

if(pid==-1){
  return -1;
}

 struct thread *child= get_thread(pid);
 child->parent = thread_current();
 //printf("%s\n",child->parent->name);
 
 list_push_back (&thread_current ()->children, &child->child_elem);



return pid;
}

pid_t syscall_wait(pid_t pid){

bool es=false;
struct thread *child=get_thread(pid);
struct thread *c=thread_current();

struct list_elem *e;

if(child==NULL){
  return -1;
              }


for (e = list_begin (&c->children); e != list_end (&c->children);
       e = list_next (e))
      {
          
        struct thread *child2 = list_entry (e, struct thread,child_elem);
   
          
          if (pid == child2->tid)
      {
      
        es=true;
        
        
      }
        }
         if(!es){
        return -1;
      }
       else{
            c->waiting_for=pid;
            
            sema_down (&c->sema_wait);
           //printf("%s\n",c->exit_status_hijo);
           //printf("%s\n",c->name);
            if (VALIDEXIT!=-10)
            {
              int b=VALIDEXIT;
              VALIDEXIT=-10;
              return b;
            }
            else{
           
              return -1;
            }
            


      }



//int x= process_wait(pid);
return 2;
}

int syscall_halt(){
 shutdown_power_off();
return 1;
}

static void
syscall_handler (struct intr_frame *f)
{
  // intr_frame holds CPU register data
  //   Intel 80386 Reference Programmer's Manual (TL;DR)
  //     http://www.scs.stanford.edu/05au-cs240c/lab/i386/toc.htm

  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)
  int syscall_number = (*p);

  switch(syscall_number)
  {
    case SYS_HALT:
      //printf("system call: halt\n");
      syscall_halt();
      break;

    case SYS_EXIT:
      p++;
    int status=(*p);

    syscall_exit(status);
      break;

    case SYS_EXEC:
       p++;
      char *name=(*p);
      //printf("%s\n",name );
      f->eax =syscall_execute(name);
      return;
      break;

    case SYS_WAIT:
       p++;
    int pid= (*p);

   f->eax =syscall_wait (pid);
    return;

    case SYS_WRITE:
      syscall_write(f);
      return;

    default:
      printf("system call: unhandled syscall. Terminating process[%d]\n",
             thread_current()->tid);
      break;
  }

  // Syscall handling failed, terminate the process
  thread_exit ();
}



void
syscall_init (void)
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

